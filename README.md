Role Name
=========

Deploy a Samba fileserver joined to an Active Directory domain

Requirements
------------

To use this role these variables have to be present:
```yaml
samba_ad_domain: # The simple windows domain. E.g. SAMDOM
samba_ad_realm: # The fully qualified AD domain name. E.g. SAMDOM.TOPDOMAIN.ORG
samba_ad_join_user: # The user account with the rights to add new computers to the domain in de samba_ad_join_ou variable
samba_ad_join_password: # The join account user password
samba_ad_join_ou: # The OU in which to create the new computer in reverse LDAP order. E.g. Servers/Servers/Fileservers (which is in LDAP OU=Fileservers,OU=Servers,OU=Servers,DC=SAMDOM,DC=TOPDOMAIN,DC=ORG)
samba_ad_share_admin_group: # The name of the fileserver admins for this share in windows notation. E.g. SAMDOM\Domain Admins

# When offering a local disk as a share
samba_share_name: # Name of the share e.g. groups, homedir
samba_root_folder: # The path on the filesystem to share

# When offering an S3 bucket
samba_s3_shares:
  - name: # Name of the share to offer. Results in \\servername\share_name
    root_folder: # The path on the filesystem to share.
    mount_folder: #  The mountfolder for the S3 bucket
    bucket_name: # Name of the S3 bucket 
    aws_access_key_id: # Credentials of the user allowed to access the bucket
    aws_secret_access_key: # Credentials of the user allowed to access the bucket
    read_only: # default value is no, optionally can be set to yes
    group_name: # (optional) The allowed group to acces this share, will be added with AD\admin - fileshares for access
```

Optional
--------

These settings can be set optionally

```yaml
samba_netbios_name: #Is the hostname is over 15 characters long, set this to a value that has less or equal to 15 charactars
samba_hosts_dns_servers: # Add one or several to override any DHCP settings. To join a domain the DNS servers should be the onse that contain the SRV records for that domain.
  - name: #dns name of the dns server
    ip_address: #IP address of the dns server
```

Role Variables
--------------

See defaults/main.yml for settable variables and it's defaults

Dependencies
------------

Fileservers should have access to the AD DNS for this to work, especially the SRV records for service discovery.

Firewall settings should enable port TCP/139 and TCP/445 for access to the fileshare

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

``` yaml
- name: Install Samba file server
  hosts: all
  become: true
  tags: always
  tasks:
    - name: Apply samba fileserver role
      include_role:
        name: samba
        apply:
          tags: samba
```

